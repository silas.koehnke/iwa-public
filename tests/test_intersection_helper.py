import unittest
from helpers.intersection_helper import overlap, calculate_date_overlap, do_projects_overlap, Is_fully_contained, GetOverlappingBlocksIndices
from models.Project import Project

class TestIntersectionHelper(unittest.TestCase):

    def setUp(self):
        # Create some mock projects to use in tests
        self.project1 = Project(1, "Project 1", 0, 0, 10, 10, 1, 10, False, False, False)
        self.project2 = Project(2, "Project 2", 5, 5, 10, 10, 1, 10, False, False, False)
        self.project3 = Project(3, "Project 3", 20, 20, 10, 10, 20, 30, False, False, False)
        # Add more projects as needed for thorough testing

    def test_overlap(self):
        # Test for projects with overlap
        self.assertEqual(overlap(self.project1, self.project2), 250) # Assuming a certain expected overlap value
        # Test for projects without overlap
        self.assertEqual(overlap(self.project1, self.project3), 0)

    def test_calculate_date_overlap(self):
        # Test for projects with date overlap
        self.assertEqual(calculate_date_overlap(self.project1, self.project2), 10) # Assuming a certain expected overlap value
        # Test for projects without date overlap
        self.assertEqual(calculate_date_overlap(self.project1, self.project3), 0)

    def test_do_projects_overlap(self):
        # Test for projects with overlap
        self.assertTrue(do_projects_overlap(self.project1, self.project2))
        # Test for projects without overlap
        self.assertFalse(do_projects_overlap(self.project1, self.project3))

    def test_is_fully_contained(self):
        # Assuming container coordinates and size
        container_x, container_y, container_width, container_length = 0, 0, 20, 20
        # Test for project fully contained
        self.assertTrue(Is_fully_contained(container_x, container_y, container_width, container_length, self.project1))
        # Test for project not fully contained
        self.assertFalse(Is_fully_contained(container_x, container_y, container_width, container_length, self.project3))

    def test_get_overlapping_blocks_indices(self):
        projects = [self.project1, self.project2, self.project3]
        # Test for getting overlapping indices
        self.assertEqual(GetOverlappingBlocksIndices(projects, 0), [1])
        # Test for a project with no overlaps
        self.assertEqual(GetOverlappingBlocksIndices(projects, 2), [])

if __name__ == '__main__':
    unittest.main()
