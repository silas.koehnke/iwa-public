import time, copy, random
from helpers.gls_helper import find_max_utility_pair_indices, f, h, h_partial, build_activeList, build_positionList
from helpers.plot_helper import plot_current_values
import matplotlib.pyplot as plt
from IPython.display import clear_output
from typing import List
from models.Project import Project

class GuidedLocalSearch:
    """Implmentation of the Guided Local Search algorithm."""
    def __init__(self, lambda_value: float, width: float, length: float):
            self.objective_function_values: List[float] = []
            self.iteration_counts: List[int] = []
            self.overlap_terms: List[float] = []
            self.penalty_terms: List[List[int]] = []
            self.lambda_value: float = lambda_value
            self.WIDTH: float = width
            self.LENGTH: float = length

    def run_gls(self, initial_solution: List[Project], time_limit: float, should_plot: bool=False) -> List[Project]:
        """Run the Guided Local Search algorithm.
        
        Args:
            initial_solution (List[Project]): The initial solution to start the algorithm with.
            time_limit (float): The time limit in seconds.
            should_plot (bool): Whether the algorithm should plot the objective function values in the time domain."""
        # Initialize initial and best solution
        current_solution = initial_solution
        best_solution = copy.deepcopy(initial_solution)
        # Initialize runtime
        t = 0
        start_time = time.time()
        previous_elapsed_seconds = -1
        # Initialize penalties to 0 for all pairs of blocks
        penalties = [[0 for j in range(len(initial_solution))] for i in range(len(initial_solution))]
        
        trys = 0 
        print(f"initial solution: {h(initial_solution, penalties, self.lambda_value)}")
        iteration_number = 0
        
        while h(current_solution, penalties, self.lambda_value) !=0 and t < time_limit:
            # Select a pair (i, j ) with maximum utility;
            i, j = find_max_utility_pair_indices(current_solution, penalties)
            
            # Icrease the penalty of pair (i, j )
            penalties[i][j] += 1
            # Run FLS with new penalties
            current_solution = self.run_fls(copy.deepcopy(current_solution), i, j, penalties)
    
            if(h(current_solution, penalties, self.lambda_value) < h(best_solution, penalties, self.lambda_value)):
                print(f"BEST SOLUTION CHANGED: current {h(current_solution, penalties, self.lambda_value)} best {h(best_solution, penalties, self.lambda_value)}")
                best_solution = copy.deepcopy(current_solution)
            
            trys+=1
            t = time.time() - start_time  
            
            if should_plot is True:
                iteration_number+=1
                current_objective_function_values = h(current_solution, penalties, self.lambda_value, self.overlap_terms, self.penalty_terms, True)

                # Append the fitness value and iteration count to the lists
                self.objective_function_values.append(current_objective_function_values)
                self.iteration_counts.append(iteration_number)  # You need to track the iteration number
                
                # Check if it's the first iteration, and clear the previous plot if so
                if iteration_number == 1:
                    plt.clf()  # Clear the figure before creating a new plot
                plot_current_values(self.iteration_counts, self.objective_function_values, self.overlap_terms, self.penalty_terms)
            else:
                elapsed_seconds = int(t)
                if elapsed_seconds != previous_elapsed_seconds:
                    clear_output(wait=True)  # Clear the previous printed output
                    print(f"Elapsed time: {elapsed_seconds} seconds")
                    previous_elapsed_seconds = elapsed_seconds
                    print(f"best solution (h): {h(best_solution, penalties, self.lambda_value)}")
                    print(f"current solution (h): {h(current_solution, penalties, self.lambda_value)}")
                    print(f"current solution (f): {f(current_solution)}\n")
                
        sep = "-"
        print(sep*80)
        print(f"best solution overall (h): {h(best_solution, penalties, self.lambda_value)}")
        print(f"best solution overall (f): {f(best_solution)}")
        print(f"Trys: {trys}")
        return best_solution

    def run_fls(self, current_solution: List[Project], i: int, j: int, penalties: List[List[int]]) -> List[Project]:
        """Run the Fast Local Search procedure.
        
        Args:
            current_solution (List[Project]): The current solution.
            i (int): The index of the first project of the last penalized pair.
            j (int): The index of the second project of the last penalized pair.
            penalties (List[List[int]]): The penalties for each pair of projects.
            
        Returns:
            List[Project]: The new solution after running the Fast Local Search procedure.
        """
        # List of the variables associated with the moves applicable to blocks i and j , 
        # and to the blocks overlapping either i or j
        ActiveList = build_activeList(current_solution, i, j)
        while len(ActiveList) > 0:
            index, property_name = random.choice(ActiveList)
            m_value = getattr(current_solution[index], property_name)
            PositionList = build_positionList(current_solution, index, current_solution[index], property_name, self.WIDTH, self.LENGTH)

            # Randomly shuffle the elements of PositionList
            random.shuffle(PositionList)
            # for k in PositionList:
            for k in PositionList:
                if (h_partial(current_solution, index, k, property_name) <=
                    h_partial(current_solution, index, m_value, property_name)):
                    m_value = k
                    
            # Let X' be the solution obtained by setting m := m∗ in X
            newpossiblesolution = copy.deepcopy(current_solution)
            setattr(newpossiblesolution[index], property_name, m_value)

            if(h(newpossiblesolution, penalties, self.lambda_value) < h(current_solution, penalties, self.lambda_value)):
                # {Execute the move}
                current_solution = newpossiblesolution
            else:
                # Remove m from ActiveList {No improvement}
                ActiveList.remove((index, property_name))
        
        # Implement the Fast Local Search procedure to explore neighborhoods
        return current_solution

    # Main function to use GLS for project scheduling
    def schedule_projects(self, projects: List[Project], time_limit:int, is_random: bool=True,  should_plot: bool=True) -> List[Project]:
        """Schedule the projects using the Guided Local Search algorithm.
        
        Args:
            projects (List[Project]): The projects to schedule.
            time_limit (int): The time limit in seconds.
            is_random (bool): Whether the initial solution should be randomly positioned inside the container dimensions before running the algorithm.
            should_plot (bool): Whether the algorithm should plot the objective function values in the time domain.
            
        Returns:
            List[Project]: The scheduled projects.
        """
        initial_solution = copy.deepcopy(projects)
        # Add your initial scheduling strategy here (random scheduling, etc.)
        if is_random is True:
            for project in initial_solution:
                # only for non static projects
                if project.isStatic is False:
                    project.x = random.randint(0, int(self.WIDTH - project.width))
                    project.y = random.randint(0, int(self.LENGTH - project.length))

        print(f"Starting GLS with {len(initial_solution)} projects:\n")
        # Apply GLS with FLS
        final_solution = self.run_gls(initial_solution, time_limit, should_plot)

        return final_solution