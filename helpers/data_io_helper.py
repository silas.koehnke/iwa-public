import csv
import datetime
from models.Project import Project
from typing import List

def import_projects(filePath: str) -> List[Project]:
    """Import projects from a CSV file."""
    # Parse the CSV data
    projects = []
    with open(filePath, 'r', encoding='utf-8-sig') as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=',')    
        for row in csv_reader:
            p = Project(
                row['ID'],
                row['Name'],
                float(row['X']),
                float(row['Y']),
                float(row['Length']),
                float(row['Width']),
                int(row['StartDateInt']),
                int(row['EndDateInt']),
                row['HasCollision'] == 'True',
                row['IsStatic'] == 'True',
                row['IsScenario'] == 'True',
            )
            projects.append(p)

    print(f"Loaded {len(projects)} projects.\n")
    return projects

def import_projects_with_static_objects(filePath: str) -> List[Project]:
    """Import projects from a CSV file and append static objects."""

    # Load the projects
    projects = import_projects(filePath)

    # Append static objects and set the end date to the maximum end date of all projects
    static_objects = import_projects('./data/staticobjects.csv')
    max_date = max([p.end_date for p in projects])
    for p in static_objects:
        p.end_date = max_date
    projects.extend(static_objects)

    return projects

def export_projects(projects: List[Project], filePath: str, withTimestamp: bool=True) -> None:
    """Export projects to a CSV file."""
    # Create the filename with the time and date
    if withTimestamp:
        current_time = datetime.datetime.now()
        time_string = current_time.strftime("%Y-%m-%d_%H-%M-%S")
        filePath = f"{filePath}_{time_string}.csv"

    # Write the data to the CSV file
    with open(filePath, 'w', newline='') as csv_file:
            csv_writer = csv.writer(csv_file, delimiter=',')

            # Write the header row
            header = ["ID", "Name", "X", "Y", "Length", "Width", "StartDateInt", "EndDateInt", 'HasCollision', 'IsStatic', 'IsScenario']
            csv_writer.writerow(header)

            # Write data for each project
            for project in projects:
                row = [
                    project.project_id,
                    project.name,
                    project.x,
                    project.y,
                    project.length,
                    project.width,
                    project.start_date,
                    project.end_date,
                    project.hasCollision,
                    project.isStatic,
                    project.isScenario
                ]
                csv_writer.writerow(row)