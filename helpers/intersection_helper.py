from models.Project import Project
from typing import List

def overlap(project1: Project, project2: Project) -> float:
    """Calculate the volume overlap between two projects based on x and y coordinates and the time dimension."""
    # Calculate the volume overlap between two projects based on x and y coordinates
    overlap_in_x = max(0, min(project1.x + project1.width, project2.x + project2.width) - max(project1.x, project2.x))
    overlap_in_y = max(0, min(project1.y + project1.length, project2.y + project2.length) - max(project1.y, project2.y))
    overlap_in_dates = calculate_date_overlap(project1, project2)

    factor = 1
    # NOTE: It is possible to use a factor other than 1 to avoid collisions with static objects with higher priority.
    # if project1.isStatic or project2.isStatic:
    #     factor = 1

    # overlap_dates = max(0, min(project1.end_date, project2.end_date) - max(project1.start_date, project2.start_date))
    return overlap_in_x * overlap_in_y * overlap_in_dates * factor

def calculate_date_overlap(project1: Project, project2: Project) -> int:
    """Calculate the overlap between two projects only with regards to the time dimension."""
    # Check if there is no overlap
    if project2.start_date > project1.end_date or project1.start_date > project2.end_date:
        return 0
    else:
        # Calculate the overlap
        overlap_start = max(project1.start_date, project2.start_date)
        overlap_end = min(project1.end_date, project2.end_date)
        # If the start date of project2 is equal to the end date of project1, consider it as an overlap of 1
        return overlap_end - overlap_start + 1

def do_projects_overlap(project1: Project, project2: Project) -> bool:
    """Check if two projects overlap in all dimensions."""
    # Check for overlap in x, y, and time
    x_overlap = project1.x + project1.width > project2.x and project2.x + project2.width > project1.x
    y_overlap = project1.y + project1.length > project2.y and project2.y + project2.length > project1.y
    time_overlap = project1.start_date <= project2.end_date and project2.start_date <= project1.end_date
    
    # If there is overlap in all dimensions, return True; otherwise, return False
    return x_overlap and y_overlap and time_overlap

def Is_fully_contained(container_x: float, container_y: float, container_width: float, container_length: float, project: Project) -> bool:
    """Check if a project is fully contained in a container based on x and y coordinates."""
    # Calculate the coordinates of the container's boundaries
    container_right = container_x + container_width
    container_top = container_y + container_length

    # Calculate the coordinates of the project's boundaries
    project_right = project.x + project.width
    project_top = project.y + project.length

    # Check if the project is fully contained in the container
    return (container_x <= project.x and container_y <= project.y and
            container_right >= project_right and container_top >= project_top)

def GetOverlappingBlocksIndices(projects: List[Project], i: int) -> List[int]:
    """Get the indices of all projects that overlap with a given project."""
    output = []
    for j, p in enumerate(projects):
        if(j == i):
            continue
        if(do_projects_overlap(p, projects[i])):
            output.append(j)
    return output

def GetOverlappingBlocksIndicesByDate(projects: List[Project], i: int) -> List[int]:
    """Get the indices of all projects that overlap with a given project only with regards to the time dimension."""
    output = []
    for j, p in enumerate(projects):
        if(j == i):
            continue
        if(calculate_date_overlap(p, projects[i]) > 0):
            output.append(j)
    return output