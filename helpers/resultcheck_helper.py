from models.Project import Project
from helpers.intersection_helper import do_projects_overlap
from typing import List
from algorithms.GuidedLocalSearch import GuidedLocalSearch
from helpers.plot_helper import plot_solution_interactive_2D, plot_solution_3D

def update_has_collision_values(projects: List[Project]) -> int:
    """Update the hasCollision values for all projects."""
    # reset all collisions
    for p in projects:
        p.hasCollision = False

    n = len(projects)
    overlaps = []
    for i in range(n - 1):
        for j in range(i + 1, n):
            project1 = projects[i]
            project2 = projects[j]
            if do_projects_overlap(project1, project2):
                # set collisions
                project1.hasCollision = True
                project2.hasCollision = True
                overlaps.append((i, project1.name, j, project2.name))

    print("Collisions:")
    for o in overlaps:
        print(o)

    total_overlapping_projects = sum(p.hasCollision for p in projects)
    print(f"Total overlapping projects: {total_overlapping_projects}")
    return total_overlapping_projects

def print_overlaps(projects: List[Project]) -> None:
    """Print all projects with collisions."""
    for p in projects:
        if p.hasCollision:
            print(p)

def filter_collisions(projects: List[Project], static_only: bool=False) -> List[Project]:
    """Filter projects with collisions."""
    if static_only:
        return [project for project in projects if project.hasCollision and project.isStatic]
    else:
        return [project for project in projects if project.hasCollision]

def test_solution_for_collisions(gls: GuidedLocalSearch, final_solution: List[Project], plot_collisions: bool=False):
    """Test a solution by asserting that there are no collisions."""
    # Find remaining collisions
    static_objects_with_collision = filter_collisions(final_solution, static_only=True)
    all_objects_with_collision = filter_collisions(final_solution, static_only=False)

    # Assert and report collisions with static and all objects
    assert len(static_objects_with_collision) == 0, f"❌ Test case failed: Collisions with static objects detected: {static_objects_with_collision}"
    assert len(all_objects_with_collision) == 0, f"❌ Test case failed: Collisions detected: {all_objects_with_collision}"

    # Plotting if required and if there are collisions
    if plot_collisions and len(all_objects_with_collision) > 0:
        plot_solution_interactive_2D(all_objects_with_collision, gls.WIDTH, gls.LENGTH)    
        plot_solution_3D(all_objects_with_collision, gls.WIDTH, gls.LENGTH)
        print_overlaps(final_solution)

    if len(all_objects_with_collision) == 0:
        print("✅ Test case passed: There are no collisions in the solution.")
