from helpers.intersection_helper import overlap, do_projects_overlap, GetOverlappingBlocksIndices, GetOverlappingBlocksIndicesByDate
from models.Project import Project
from typing import List, Tuple
import copy

def I(project1: Project, project2: Project) -> int:
    """Indicator function that returns 1 if two projects overlap and 0 otherwise."""
    if(do_projects_overlap(project1, project2)):
        return 1
    else:
        return 0

def find_max_utility_pair_indices(projects: List[Project], penalties: List[List[int]]) -> Tuple[int, int]:
    """Find the indices of the project pair with the maximum utility value."""
    max_pair_indices = max(
        ((i, j) for i, project1 in enumerate(projects) for j, project2 in enumerate(projects) if i < j),
        key=lambda indices: overlap(projects[indices[0]], projects[indices[1]]) / (1 + penalties[indices[0]][indices[1]]),
        default=None
    )
    return max_pair_indices

def f(projects: List[Project]) -> float:
    """Calculate the objective function value of a given solution."""
    total_overlap = 0
    n = len(projects)
    # Calculate total overlap between scheduled projects
    for i in range(n - 1):
        for j in range(i + 1, n):
            total_overlap += overlap(projects[i], projects[j])
    return total_overlap

def h(projects: List[Project], penalties: List[List[int]] , lambda_value: float, overlap_terms: List[float] = [], penalty_terms: List[float] = [], append: bool = False) -> float:
    """Calculate the augmented objective function value of a given solution."""
    total_overlap = 0
    total_penalty_term = 0
    n = len(projects)

    # Calculate total overlap between scheduled projects
    for i in range(n - 1):
        for j in range(i + 1, n):
            total_overlap += overlap(projects[i], projects[j])

    # Calculate penalty term
    for i in range(n - 1):
        for j in range(i + 1, n):
            total_penalty_term += penalties[i][j] * I(projects[i], projects[j])
    
    # Append the overlap and penalty terms to the given lists. This is used for plotting the objective function value in the time domain.
    if append:
        overlap_terms.append(total_overlap)
        penalty_terms.append(lambda_value * total_penalty_term)

    # Calculate the objective function with the given lambda
    return total_overlap + lambda_value * total_penalty_term

def h_partial(projects: List[Project], p_index: int, value: float, property_name: str) -> float:
    """Calculate the partial augmented objective function value of a given solution. """
    projects_copy = copy.deepcopy(projects)
    if property_name == 'start_date':
        projects_copy[p_index].start_date += value
        projects_copy[p_index].end_date += value
    else:
        setattr(projects_copy[p_index], property_name, value)
    
    total_overlap = 0
    n = len(projects_copy)

    # Calculate total overlap between scheduled projects
    for i in range(n):
        if(i == p_index):
            continue
        total_overlap += overlap(projects_copy[p_index], projects_copy[i])
    return total_overlap

def build_activeList(projects: List[Project], i: int, j: int) -> List[Tuple[int, str]]:
    """Build the list of active sub-neighbourhoods for a chosen project pair with maximum utility value. 

    Args:
    projects (List[Project]): The list of projects.
    i (int): The index of the first project of the last penalized pair.
    j (int): The index of the second project of the last penalized pair.

    Returns:
    This produces a list of tuples, where each tuple contains the index of a project and the name of the property to be changed."""
    ActiveList = []
    overlaps_with_i = GetOverlappingBlocksIndices(projects, i)
    overlaps_with_j = GetOverlappingBlocksIndices(projects, j)
    relevant = set(overlaps_with_i + overlaps_with_j + [i, j])

    # Dont consider moving static objects
    non_static_objects = [x for x in relevant if projects[x].isStatic == False]

    # Convert the set back to a list if needed
    relevant_indices = list(non_static_objects)
    
    for r in non_static_objects:
        ActiveList.append((r, "x"))
        ActiveList.append((r, "y"))
        # ActiveList.append((r, "start_date"))
    
    return ActiveList
        
def build_positionList(projects: List[Project], i: int, project: Project, property_name: str, container_width: float, container_length: float) -> List[float]:	
    """Build the list of possible positions for a chosen project and property. 
    Args:
    projects (List[Project]): The list of projects.
    i (int): The index of the chosen project.
    project (Project): The chosen project.
    property_name (str): The name of the property to be changed.
    container_width (float): The width of the container (assembly area).
    container_length (float): The length of the container (assembly area).
    
    Returns: 
    This produces a list of possible values for the given property."""
    # overlaps = [index for index in range(len(projects))]
    overlaps = GetOverlappingBlocksIndicesByDate(projects, i)
    # overlaps = GetOverlappingBlocksIndices(projects, i)
    out = []
    match property_name:
        case 'x':
            for overlap_index in overlaps:
                # Breakpoints
                p = projects[overlap_index]
                out.append(p.x + p.width)
                out.append(p.x - project.width)
            # Rand-Werte
            out.append(0)
            out.append(container_width - project.width)

            out = [x for x in out if 0 <= x <= container_width - project.width]
            out = set(out)
            out = list(out)
        case 'y':
            for overlap_index in overlaps:
                # Breakpoints
                p = projects[overlap_index]
                out.append(p.y + p.length)
                out.append(p.y - project.length)
            # Rand-Werte
            out.append(0)
            out.append(container_length - project.length)
            out = [y for y in out if 0 <= y <= container_length - project.length]
            out = set(out)
            out = list(out)
        case 'start_date':
            for date in range(-5, 6):
                out.append(date)
        case _:
            return out
    return out