import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from ipywidgets import interact, widgets, fixed
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from IPython.display import clear_output
from models.Project import Project
import random
from typing import List

def plot_solution_interactive_2D(projects_list: List[Project], container_width: float, container_length: float) -> None:
    """Plot the projects as an interactive 2D plot. The time can be chosen with a slider."""
    # Create a slider to choose the time
    time_slider = widgets.IntSlider(
        value=min(project.start_date for project in projects_list),
        min=min(project.start_date for project in projects_list),
        max=max(project.end_date for project in projects_list if project.isStatic == False),
        step=1,
        description='Zeitpunkt (Woche):',
    )

    # Create an interactive plot
    interact(plot_projects, chosen_time=time_slider, container_width=fixed(container_width), container_length=fixed(container_length), projects_list=fixed(projects_list))

def plot_projects(chosen_time: int, container_width: float, container_length: float, projects_list=None) -> None:
    """Callback for plotting the projects at a given time. This function is called by the interactive plot."""
    fig, ax = plt.subplots(figsize=(8, 6))
    n = 0
    # Plot the projects as rectangles
    for index, project in enumerate(projects_list):
        if project.start_date <= chosen_time <= project.end_date:
            x = project.x
            y = project.y
            rect = Rectangle((x, y), project.width, project.length, fill=True, edgecolor='black')
            if project.isStatic:
                rect.set_facecolor('orange')
            elif project.hasCollision:
                rect.set_facecolor('red')
            elif project.isScenario:
                rect.set_facecolor('yellow')
            ax.add_patch(rect)
            
            # Calculate the center of the rectangle
            center_x = x + project.width / 2
            center_y = y + project.length / 2
            
            # Add the project index as text in the center of the rectangle
            ax.text(center_x, center_y, str(index), ha='center', va='center')
            
            n +=1
    # Plot the container as a rectangle
    container = Rectangle((0, 0), container_width, container_length, fill=False, edgecolor='green')
    ax.add_patch(container)
    
    # Set plot properties
    ax.set_aspect('equal')
    plt.xlim(-5, container_width + 10)
    plt.ylim(-5, container_length + 10)
    ax.invert_yaxis()
    plt.show()

def plot_solution_3D(projects: List[Project], container_width: float, container_length: float) -> None:
    """Plot the projects as a 3D plot. The 2 x and y axes represent the spatial coordinates, and the z axis represents the time."""
    # Create a 3D plot
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    # Plot the projects as cuboids
    for project in projects:
        x = project.x
        y = project.y
        z_start = project.start_date
        z_end = project.end_date
        length = project.length
        width = project.width

        if project.isStatic:
            color = 'orange'
        elif project.hasCollision:
            color = 'red'
        elif project.isScenario:
            color = 'yellow'
        else:
            color = (random.random(), random.random(), random.random())

        ax.bar3d(x, y, z_start, width, length, z_end - z_start, shade=True, color=color)

    # Plot the transparent 3D container
    container_height = max(project.end_date for project in projects if project.isStatic == False) +1  # A large value for infinite height
    container_vertices = [
    [0, 0, 0],
    [container_width, 0, 0],
    [container_width, container_length, 0],
    [0, container_length, 0],
    [0, 0, container_height],
    [container_width, 0, container_height],
    [container_width, container_length, container_height],
    [0, container_length, container_height],
    ]
    container_faces = [
        [container_vertices[0], container_vertices[1], container_vertices[2], container_vertices[3]],
        [container_vertices[4], container_vertices[5], container_vertices[6], container_vertices[7]],
        [container_vertices[0], container_vertices[1], container_vertices[5], container_vertices[4]],
        [container_vertices[2], container_vertices[3], container_vertices[7], container_vertices[6]],
        [container_vertices[1], container_vertices[2], container_vertices[6], container_vertices[5]],
        [container_vertices[0], container_vertices[3], container_vertices[7], container_vertices[4]],
    ]
    ax.add_collection3d(Poly3DCollection(container_faces, facecolors='cyan', linewidths=1, edgecolors='g', alpha=0.05))

    # Set plot properties
    ax.set_xlim(0, container_width + 10)
    ax.set_ylim(0, container_length + 10)
    ax.set_zlim(0, container_height + 10)
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Zeit')
    plt.show()

def plot_current_values(iteration_counts: List[int], objective_function_values: List[float], overlap_terms: List[float], penalty_terms: List[float]) -> None:
    """Plot the objective function values of the best solution in the time domain."""
    # Update the plot
    plt.figure("Zielfunktionswerte: ", figsize=(10, 4))
    plt.plot(iteration_counts, objective_function_values,'b-', label='Zielfunktion h(x)')
    plt.plot(iteration_counts, overlap_terms, 'r-', label='overlap terms')
    plt.plot(iteration_counts, penalty_terms, 'g-', label='penalty terms')
    plt.xlabel('Iteration')
    plt.ylabel('Wert')
    plt.title('Zielfunktionswerte der besten Lösung im Zeitverlauf')
    plt.grid(True)
    plt.legend()

    # Clear the previous plot and display the updated plot
    clear_output(wait=True)
    plt.show()