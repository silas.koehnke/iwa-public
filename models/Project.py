class Project:
    """A class to represent a project.
    
    Attributes:
        project_id (int): The id of the project.
        name (str): The name of the project.
        x (float): The x coordinate of the project.
        y (float): The y coordinate of the project.
        length (float): The length of the project.
        width (float): The width of the project.
        start_date (float): The start date of the project.
        end_date (float): The end date of the project.
        hasCollision (bool): Whether the project has a collision.
        isStatic (bool): Whether the project is a static object (e.g. permanent blocked area).
        isScenario (bool): Whether the project is a scenario.
    """
    def __init__(self, project_id: int,
                 name: str,
                 x: float,
                 y: float,
                 length: float,
                 width: float,
                 start_date: int,
                 end_date: int,
                 hasCollision: bool,
                 isStatic: bool,
                 isScenario: bool):
        self.project_id = project_id
        self.name = name
        self.x = x
        self.y = y
        self.length = length
        self.width = width
        self.start_date = start_date
        self.end_date = end_date
        self.hasCollision = hasCollision
        self.isStatic = isStatic
        self.isScenario = isScenario

    def __str__(self) -> str:
        """Return the string representation of the project."""
        return (
            f"ID: {self.project_id}\n"
            f"Name: {self.name}\n"
            f"X: {self.x}\n"
            f"Y: {self.y}\n"
            f"Length: {self.length}\n"
            f"Width: {self.width}\n"
            f"Start Date: {self.start_date}\n"
            f"End Date: {self.end_date}\n"
            f"Has Collision: {self.hasCollision}\n"
            f"Is Static: {self.isStatic}\n"
            f"Is Scenario: {self.isScenario}\n"
        )